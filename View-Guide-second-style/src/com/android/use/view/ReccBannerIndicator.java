package com.android.use.view;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.android.use.view.R;

public class ReccBannerIndicator extends View {
	private static final String TAG = "ReccBannerIndicator";

	private Resources mRes;
	private Drawable mNormalDrawable;
	private Drawable mSelectedDrawable;
	private ViewPager mViewPager;
	private int mCurrentPage;
	private int mHorizontalSpan;

	public ReccBannerIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public ReccBannerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ReccBannerIndicator(Context context) {
		super(context);
		init();
	}

	private void init() {
		mRes = getResources();
		mCurrentPage = -1;
		mNormalDrawable = mRes
				.getDrawable(R.drawable.recc_banner_indicator_normal);
		mSelectedDrawable = mRes
				.getDrawable(R.drawable.recc_banner_indicator_selected);
		mHorizontalSpan = mRes
				.getDimensionPixelSize(R.dimen.recc_banner_indicator_span);
	}

	private ArrayList<Rect> calcAllBounds() {
		int count = mViewPager.getAdapter().getCount();
		ArrayList<Rect> bounds = new ArrayList<Rect>(count);
		int width = getWidth();
		int height = getHeight();
		Rect rect = null;
		int drawableWidth = mNormalDrawable.getIntrinsicWidth();
		int drawableHeight = mNormalDrawable.getIntrinsicHeight();
		/**
		 * 口 口 口
		 * */
		int totalDrawableWidth = drawableWidth * count + mHorizontalSpan
				* (count - 1);
		for (int i = 0; i < count; i++) {
			rect = new Rect();
			rect.left = (width - totalDrawableWidth) / 2 + i
					* (drawableWidth + mHorizontalSpan);
			rect.top = (height - drawableHeight) / 2;
			rect.right = rect.left + drawableWidth;
			rect.bottom = rect.top + drawableHeight;
			bounds.add(rect);
		}

		return bounds;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mViewPager == null) {
			return;
		}
		int count = mViewPager.getAdapter().getCount();
		if (count == 0) {
			return;
		}
		if (mCurrentPage == -1 && mViewPager != null) {
			mCurrentPage = mViewPager.getCurrentItem();
		}
		ArrayList<Rect> bounds = calcAllBounds();
		int size = bounds.size();
		for (int i = 0; i < size; i++) {
			if (i == mCurrentPage) {
				mSelectedDrawable.setBounds(bounds.get(i));
				mSelectedDrawable.draw(canvas);
			} else {
				mNormalDrawable.setBounds(bounds.get(i));
				mNormalDrawable.draw(canvas);
			}
		}
	}

	public void setViewPager(ViewPager viewPager) {
		this.mViewPager = viewPager;
		invalidate();
	}

	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// TODO
	}

	public void onPageScrollStateChanged(int state) {
		// TODO
	}

	public void onPageSelected(int position) {
		if (mCurrentPage != position) {
			mCurrentPage = position;
			invalidate();
		}
	}
}
