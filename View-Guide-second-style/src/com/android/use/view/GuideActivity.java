package com.android.use.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class GuideActivity extends Activity implements View.OnClickListener {
	private static final String TAG = "MainActivity";

	private ViewPager mPager;
	private List<View> mBackgrounds;
	private Button mStart;
	private ReccBannerIndicator mIndicator;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.guide_layout);

		initViewPageData();

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setOnPageChangeListener(new MyPagerChangListener());
		mPager.setAdapter(new MyViewPagerAdapter(mBackgrounds));
		mStart = (Button) findViewById(R.id.start_button);
		mStart.setOnClickListener(this);
		mStart.setVisibility(View.INVISIBLE);
		mIndicator = (ReccBannerIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);
	}

	private void initViewPageData() {
		mBackgrounds = new ArrayList<View>();
		for (int i = 0; i < 3; i++) {
			ImageView iv = new ImageView(this);
			iv.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT));
			int resId = R.drawable.guide1_bg;
			if (i == 1) {
				resId = R.drawable.guide2_bg;
			} else if (i == 2) {
				resId = R.drawable.guide3_bg;
			}
			iv.setBackgroundResource(resId);
			mBackgrounds.add(iv);
		}
	}

	private class MyPagerChangListener implements
			ViewPager.OnPageChangeListener {

		AlphaAnimation alphaAnim;
		int curPosition;

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			curPosition = position;
			if (position == mBackgrounds.size() - 1) {
				if (alphaAnim == null) {
					initAnim();
				}
				mStart.startAnimation(alphaAnim);
			} else {
				mStart.setVisibility(View.GONE);
			}
			mIndicator.onPageSelected(position);
		}

		@Override
		public void onPageScrollStateChanged(int state) {
		}

		private void initAnim() {
			alphaAnim = new AlphaAnimation(0, 1.0f);
			alphaAnim.setDuration(1000);
			alphaAnim.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					if (curPosition == mBackgrounds.size() - 1) {
						mStart.setVisibility(View.VISIBLE);
					} else {
						mStart.setVisibility(View.GONE);
					}
				}
			});
		}
	}

	public class MyViewPagerAdapter extends PagerAdapter {
		private List<View> mViews;

		public MyViewPagerAdapter(List<View> views) {
			this.mViews = views;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(mViews.get(position));
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(mViews.get(position), 0);
			return mViews.get(position);
		}

		@Override
		public int getCount() {
			return mViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.start_button) {
			saveGuide();
			finish();
		}
	}

	private void saveGuide() {
		SharedPreferences sp = getSharedPreferences("guide",
				Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean("show_guide", false);
		editor.commit();
	}

	public static boolean showGuide(Context context) {
		SharedPreferences sp = context.getSharedPreferences("guide",
				Activity.MODE_PRIVATE);
		return sp.getBoolean("show_guide", true);

	}

}