package com.android.use.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;


public class GuideActivity extends Activity implements View.OnClickListener{
	private static final String TAG  ="MainActivity";

	private ViewPager mPager;
	List<ImageView> mBackgrounds;
	private Button mStart;
	private ReccBannerIndicator mIndicator;
	private int mCurrentPage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.guide_layout);
		
		mCurrentPage = 0;

		ImageView guide1 = (ImageView) findViewById(R.id.guide_bg1);
		ImageView guide2 = (ImageView) findViewById(R.id.guide_bg2);
		ImageView guide3 = (ImageView) findViewById(R.id.guide_bg3);
		ImageView guide4 = (ImageView) findViewById(R.id.guide_bg4);
		ImageView guide5 = (ImageView) findViewById(R.id.guide_bg5);
		mBackgrounds = new ArrayList<ImageView>();
		mBackgrounds.add(guide1);
		mBackgrounds.add(guide2);
		mBackgrounds.add(guide3);
		mBackgrounds.add(guide4);
		mBackgrounds.add(guide5);

		mPager = (ViewPager) findViewById(R.id.pager);
		List<View> views = new ArrayList<View>();
		for (int i = 0; i < mBackgrounds.size(); i++) {
			RelativeLayout rl = new RelativeLayout(this);
			RelativeLayout.LayoutParams ww = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			rl.setLayoutParams(ww);
			if(i < mBackgrounds.size()-1){
				rl.setClickable(true);
				rl.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if(mCurrentPage < mBackgrounds.size() - 1){
							mCurrentPage ++;
							mPager.setCurrentItem(mCurrentPage, true);
						}
					}
				});
			}

			views.add(rl);
		}

		mPager.setOnPageChangeListener(new MyPagerChangListener());
		mPager.setAdapter(new MyViewPagerAdapter(views));
		mStart = (Button) findViewById(R.id.start_button);
		mStart.setOnClickListener(this);
		mStart.setVisibility(View.INVISIBLE);
		mIndicator = (ReccBannerIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);
	}


	private class MyPagerChangListener implements ViewPager.OnPageChangeListener{

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			for (int i = 0; i < mBackgrounds.size(); i++) {
				if(i == position){
					mBackgrounds.get(i).setVisibility(View.VISIBLE);
				}else {
					mBackgrounds.get(i).setVisibility(View.INVISIBLE);
				}
			}
			if (position == mBackgrounds.size() - 1) {
				mStart.setVisibility(View.VISIBLE);
			}else{
				mStart.setVisibility(View.GONE);
			}
			mIndicator.onPageSelected(position);
			mCurrentPage = position;
		}

		@Override
		public void onPageScrollStateChanged(int state) {
		}

	}

	public class MyViewPagerAdapter extends PagerAdapter{
		private List<View> mViews;

		public MyViewPagerAdapter(List<View> views) {
			this.mViews = views;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) 	{	
			container.removeView(mViews.get(position));
		}


		@Override
		public Object instantiateItem(ViewGroup container, int position) {		
			container.addView(mViews.get(position), 0);
			return mViews.get(position);
		}

		@Override
		public int getCount() {
			return  mViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.start_button) {
				saveGuide();
				finish();
		}
	}

	private void saveGuide(){
		SharedPreferences sp = getSharedPreferences("guide", Activity.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean("show_guide", false);
		editor.commit();
	}

	public static boolean showGuide(Context context) {
		SharedPreferences sp = context.getSharedPreferences("guide", Activity.MODE_PRIVATE);
		return sp.getBoolean("show_guide", true);

	}

}